import cards from './cards';
import randomProfile from 'random-profile-generator';


const getAvatars = (amount) => {
    let res = [];
    for(let i = 0; i<amount; i++)
        res[i] = randomProfile.avatar();
    return res;
};
const getUser = () => {
    const {fullName, avatar}= randomProfile.profile();
    return {fullName, avatar};
};

export default function getJobs(){


    return cards.map( card => {
        const amount = Math.round(Math.random()*10) + 5;
        return{
            ...card, address: randomProfile.address(), userAmount: amount,avatars: getAvatars(amount), firstUser:getUser(), author: {...getUser(), phone: randomProfile.phone()}
        }
    });
};