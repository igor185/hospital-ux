const corporations = {
    DoubleSpring: {
        name: "DoubleSpring",
        avatar: "fab fa-facebook"
    },
    Stripe_Inc: {
        name: "Stripe Inc.",
        avatar: "fab fa-apple"
    },
    Google_Inc: {
        name: "Google Inc.",
        avatar: "fab fa-google"
    }
};


const status = [
    {
        name: "pending",
        avatar: "fas fa-clock-o"
    }, {
        name: "paid",
        avatar: "fas fa-money"
    }, {
        name: "submitted",
        avatar: "fas fa-paper-plane"
    }];

const abilities = ["EN", "Operation Room", "Maternity", "reception", "intensive care", "nurse", "surgeon", "physician", "dentist", "cardiologist", "urologist"];


const cards = [
    {
        created_At: "7 28 2019 04:18:00",
        data_job: "7 31 2019 18:00:00",
        time_job: ["18.00", "22.00"],
        corp: corporations.DoubleSpring,
        name: "BlackMonk",
        status: status[0],
        skills: [abilities[7], abilities[5], abilities[1]]
    },
    {
        created_At: "6 24 2019 15:14:00",
        data_job: "8 02 2019 15:00:00",
        time_job: ["15.00", "20.00"],
        corp: corporations.DoubleSpring,
        name: "BlackMonk Insider",
        status: status[1],
        skills: [abilities[1], abilities[0], abilities[4]]
    },
    {
        created_At: "7 28 2019 04:18:00",
        data_job: "8 03 2019 14:00:00",
        time_job: ["14.00", "24.00"],
        corp: corporations.Stripe_Inc,
        name: "Apply Pay on Stripe",
        status: status[2],
        skills:[abilities[4],abilities[1],abilities[2],abilities[3]]
    },
    {
        created_At: "7 28 2019 04:18:00",
        data_job: "8 24 2019 12:30:00",
        time_job: ["12.30", "14.30"],
        corp: corporations.Google_Inc,
        name: "Project Polymer",
        status: status[1],
        skills:[abilities[0],abilities[1],abilities[2]]
    },
    {
        created_At: "7 28 2019 04:18:00",
        data_job: "7 31 2019 18:00:00",
        time_job: ["18.00", "22.00"],
        corp: corporations.DoubleSpring,
        name: "BlackMonk",
        status: status[0],
        skills: [abilities[7], abilities[5], abilities[1]]
    },
    {
        created_At: "6 24 2019 15:14:00",
        data_job: "8 02 2019 15:00:00",
        time_job: ["15.00", "20.00"],
        corp: corporations.DoubleSpring,
        name: "BlackMonk Insider",
        status: status[1],
        skills: [abilities[1], abilities[0], abilities[4]]
    },
    {
        created_At: "6 24 2019 15:14:00",
        data_job: "8 03 2019 14:00:00",
        time_job: ["14.00", "24.00"],
        corp: corporations.Stripe_Inc,
        name: "Apply Pay on Stripe",
        status: status[2],
        skills:[abilities[4],abilities[1],abilities[2],abilities[3]]
    },
    {
        created_At: "7 28 2019 04:18:00",
        data_job: "8 24 2019 12:30:00",
        time_job: ["12.30", "14.30"],
        corp: corporations.Google_Inc,
        name: "Project Polymer",
        status: status[1],
        skills:[abilities[0],abilities[1],abilities[2]]
    },
];
export default cards;