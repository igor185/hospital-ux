import React from 'react';
import "./style.css";
import Header from './Header';
import getJobs from '../../data/index.js';
import CardView from '../Card/index';
import { Card } from 'semantic-ui-react'
import uuid  from 'uuid/v1';

const Body = () => {

    const cards = getJobs();

    return (
        <div className={"body"}>
            <Header/>

            <Card.Group>
                {cards.map(card => <CardView card={card} key={uuid()}/>)}
            </Card.Group>
        </div>
    )
};

export default Body;