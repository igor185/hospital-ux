import React from 'react';
import Breadcrumb from "semantic-ui-react/dist/commonjs/collections/Breadcrumb";
import Icon from "semantic-ui-react/dist/commonjs/elements/Icon";
import './style.css';

const render = () => {

    const sections = [
        {key: 'Home', content: <Icon name='archive'/>},
        {key: 'Store', content: <div>
                <span style={{fontWeight: "bold"}}>Projects</span>
                <span>Calendar</span>
                <span>Files</span>
                <span>Times</span>
            </div>},
    ];

    const BreadcrumbExampleShorthand = () => (
        <Breadcrumb icon='right angle' sections={sections}/>
    );
    return (
        <div className={"header"}>
            <div className={"header-left"}>
                {BreadcrumbExampleShorthand()}
            </div>
            <div className={"header-right"}>
                <Icon name={"search"}/>
                <Icon name={"bell"}/>
                <Icon name={"setting"}/>
                <Icon name={"help circle"}/>
            </div>
        </div>
    );
};


export default render;