import React from 'react';
import Card from "semantic-ui-react/dist/commonjs/views/Card";
import moment from 'moment';
import FontAwesome from 'react-fontawesome';
import Icon from "semantic-ui-react/dist/commonjs/elements/Icon";
import Image from "semantic-ui-react/dist/commonjs/elements/Image";
import uuid from 'uuid/v1';
import './style.css';
import Comment from "semantic-ui-react/dist/commonjs/views/Comment";
import Popup from "semantic-ui-react/dist/commonjs/modules/Popup";
import List from "semantic-ui-react/dist/commonjs/elements/List";
import Header from "semantic-ui-react/dist/commonjs/elements/Header";

const card = (props) => {
    const {corp, data_job, address, name, skills, time_job, userAmount, status, avatars, created_At, author} = props.card;

    return (
        <Card>
            <Card.Content extra>
                <Card.Header>{name}</Card.Header>
                <Card.Meta style={{display: "flex", justifyContent: "space-between", color: "#170562"}}>
                    <span><FontAwesome name={corp.avatar}/> {corp.name} </span>
                    <Popup content={new Date(data_job).toString()} trigger={<span>{moment(new Date(data_job)).fromNow()}</span>}/>
                </Card.Meta>
            </Card.Content>
            <Card.Content>
                <div className={"content-wrp"}>
                    <section>
                        <div>
                            <Icon name={"clock"}/>{time_job[0]}-{time_job[1]}
                        </div>
                        <Popup content={address}
                               trigger={<div className={"place-card"}>
                                   <Icon name={"map marker alternate"}/>
                                   <span style={{fontSize: "small"}}>{address}</span>
                               </div>}/>
                        <Header as="h3">Skills:</Header>
                        <List animated>
                            {skills.map(skill => <List.Item><span className={"skill"}>{skill}</span></List.Item>)}
                        </List>

                    </section>

                    <section>
                        <Header as={"h4"}>Author:</Header>
                        <Comment.Group>
                            <Comment>
                                <Comment.Avatar src={author.avatar}/>
                                <Comment.Content>
                                    <Comment.Author>{author.fullName}</Comment.Author>
                                    <Comment.Metadata>
                                        {author.phone}
                                    </Comment.Metadata>
                                </Comment.Content>
                            </Comment>
                        </Comment.Group>
                        <Card.Meta> <FontAwesome name={"fas fa-edit"}/>{moment(new Date(created_At)).fromNow()}
                        </Card.Meta>
                    </section>
                </div>
            </Card.Content>
            <Card.Content extra>
                <div className={"status"}>
                    <FontAwesome name={status.avatar}/>
                    <span style={{marginLeft: "5px"}}>{status.name}</span>
                </div>
                <div><Icon name={"group"}/> {userAmount} got apply</div>
                {[...avatars].splice(0, 5).map(avatar => <Image avatar src={avatar} size={"small"} key={uuid()}/>)}
                {avatars.length - 5 > 0 ?
                    <span className={"leftUsers"}>
                +{avatars.length - 5}
                </span> : null}
            </Card.Content>
        </Card>
    );
};


export default card;