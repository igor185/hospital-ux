import React from 'react';
import './style.css';
import Menu from "semantic-ui-react/dist/commonjs/collections/Menu";
import Icon from "semantic-ui-react/dist/commonjs/elements/Icon";

const Menu_wrp = () => {


    return (
        <div className={"menu-wrp"}>
            <Menu icon vertical>
                <Menu.Item className={"menu-item"}>
                    <div style={{margin: "10px 0"}}>
                        <Icon name={"hospital outline"}/>
                    </div>
                </Menu.Item>
                <Menu.Item className={"menu-item"}>
                    <Icon name='home'/>
                </Menu.Item>

                <Menu.Item active={true} className={"menu-item"}>
                    <Icon name='archive'/>
                </Menu.Item>

                <Menu.Item className={"menu-item"}>
                    <Icon name='target'/>
                </Menu.Item>

                <Menu.Item className={"menu-item"}>
                    <Icon name='group'/>
                </Menu.Item>
                <Menu.Item className={"menu-item"}>
                    <Icon name='clock outline'/>
                </Menu.Item>
                <Menu.Item className={"menu-item"}>
                    <Icon name='newspaper'/>
                </Menu.Item>
                <Menu.Item className={"menu-item"}>
                    <Icon name='chart bar'/>
                </Menu.Item>
            </Menu>
        </div>
    )
};

export default Menu_wrp;