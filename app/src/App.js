import React from 'react';
import './App.css';
import Menu from './components/Menu';
import 'semantic-ui-css/semantic.min.css';
import Body from './components/Body';

const App = () => {
    return (
        <div>
            <Menu/>
            <Body/>
        </div>
    );
};


export default App;
